/*
 * Copyright 2018 Sophos Limited. All rights reserved.
 */
package com.sandbox.git.hooks;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculationServiceTest {
    @DataProvider
    public Object[][] AddProvider()
    {
        return new Object[][]{{2, 3, 5}, {1, 6, 7}};
    }

    @Test(dataProvider = "AddProvider")
    public void AddTest(final int a, final int b, final int c)
    {
        CalculationService service = new CalculationService();
        Assert.assertEquals(c, service.add(a, b));
    }
}
