/*
 * Copyright 2018 Sophos Limited. All rights reserved.
 */
package com.sandbox.git.hooks;

public class CalculationService
{
    public int add(int a, int b)
    {
        return a + b;
    }
}
